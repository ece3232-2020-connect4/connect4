/*
 * Authors: Jermaine Andrade and Jeremy Legere
 *
 * Description: This file contains the function prototypes for
 * for the board logic component of the system. The functions
 * are based on the modules found in the FRDMK64 board. The following
 * modules are used:
 * - UART
 * - ADC
 * - DAC
 *
 * See board_logic.c for the implementations.
 */
#include "MK64F12.h"

/* ASCII character definitions */
#define NEWLINE 0xA
#define CARRIAGE_RETURN 0xD

/* Sine wave data for DAC output */
#define TOTAL_SINE_DATA_POINTS 7
#define DELAY_COUNT 500

/*
 * Sends an ASCII character to a connected serial interface
 * via the UART0 module
 *
 * @param char_to_send The character (in ASCII) to transmit
 */
void put_char(uint8_t char_to_send);

/*
 * Reads the input connected to ADC1 and returns
 * the converted value
 *
 * @return The converted value as an unsigned 8 bit number
 */
uint8_t do_adc_conversion();

/*
 * Initializes the UART0 module
 */
void init_uart();

/*
 * Initializes the ADC1 module for 8 bit conversion.
 * ADC1 is connected to PIN ADC1_SE18-17
 */
void init_adc();

/*
 * Initializes PORT C PIN 16 to be a digital input
 */
void init_ptc16_input();

/*
 * Reads the value of PORT C PIN 16 and returns it as
 * an unsigned 8 bit number
 *
 * @return The digital value read on the pin as an unsigned 8 bit number
 */
uint8_t read_ptc16();

/*
 * Outputs the provided string to a serial interface
 * based on the configuration of UART0
 *
 * @param string_to_send The string to be sent to the serial interface
 */
void put_string(char* string_to_send);

/*
 * Outputs the provided string, with a newline ending, to
 * a serial interface based on the configuration of UART0
 *
 * @param string_to_send The string to be sent to the serial interface
 */
void put_stringln(char* string_to_send);

/*
 * Initializes the DAC0 module
 * DAC0 is connected to PIN DAC0_OUT
 */
void init_dac0();

/*
 * Outputs the digital data (sine wave) provided to the DAC0 module
 * as an analog output
 *
 * @param output_count A count that represents how long the DAC should output
 */
void do_dac_conversion(int output_count);

/*
 * Delays the execution of code
 * for a given period of time
 */
void delay();
