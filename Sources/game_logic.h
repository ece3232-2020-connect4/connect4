/*
 * Authors: Jermaine Andrade and Jeremy Legere
 *
 * Description: This file contains the function prototypes for
 * for the game logic component of the system. These functions
 * are for playing the game Connect 4 using hardware components
 * connected to the FRDMk64 board.
 *
 * See main.c for the implementations.
 */

/* Game board size specifications*/
#define BOARD_WIDTH 7
#define BOARD_HEIGHT 6

/* Represents a player's move for a given turn*/
typedef struct board_position {
	int row;
	int column;
} position_t;

/*
 * Displays a banner containing the game's title
 */
void display_game_banner();

/*
 * Displays the connect 4 game board in its current state
 */
void display_game_board();

/*
 * Displays the following game state information:
 * - current player's turn
 * - turn number
 * - game board
 */
void display_game_state();

/*
 * Displays a message indicating which player won the game
 */
void display_win_message();

/*
 * Initializes the game by setting up the various hardware
 * components and any game logic
 */
void init_game();

/*
 * Determines the selected column by dividing an expected
 * voltage reading (between 0 - 255 ) into 7 values
 *
 * @param voltage An 8 bit voltage reading value
 * @return A value between 0 - 6 representing a board column
 */
int determine_selected_column(unsigned char voltage);

/*
 * Checks if the column select button is currently
 * being pressed by the user
 *
 * @return 0 if the button is not pressed, 1 if the button is pressed
 */
int check_column_select_pressed();

/*
 * Determine the player's chosen move based on the column
 * selected and the lowest available row in that column
 *
 * @param column The column, value between 0 to 6, selected by the user
 * @return The position struct of the available. The struct will
 * contain {row = -1, column = -1} if no move is available for the given column.
 */
position_t determine_chosen_move(int column);

/* Checks if the position entered is a valid move
 * given the current board state
 *
 * @param position A position struct for the player's move
 * @return 0 if the move is not valid, 1 if the move is valid
 */
int check_valid_move(position_t position);

/*
 * Gets the player's column choice for a given turn
 * by waiting for the player to move to a given column
 * and confirming the choice using the column select button
 *
 * @return A value between 0 - 6 representing the player's selected column
 */
int get_column_choice();

/*
 * Determines the player's move (row and column choice) for a given turn
 *
 * @return A position struct representing the move for the given turn
 */
position_t take_player_turn();

/*
 * Updates the game board based on the given move
 * and the player (1 or 2) who made the move
 *
 * @param move A position struct representing the valid move selected
 */
void update_game_board(position_t move);

/*
 * Checks if a player has won the game by placing 4
 * of their tiles consecutively in horizontal, vertical
 * or diagonal orientation
 *
 * @return 0 if the current player has not won the game, 1 if the current player has won the game
 */
int check_win_conditions();

/*
 * Prompts and then waits for the user to press
 * the column select button to indicate they
 * want to reset the game
 */
void wait_game_reset();

/*
 * Resets the game back to its initial state and
 * alerts the user that the game has been reset
 */
void reset_game_state();

/*
 * Triggers the buzzer to play for a
 * certain period of time
 */
void play_buzzer();

/*
 * Triggers a series of events to signify a player has won:
 * - displays game board state (see winning move)
 * - displays a message indicating which player won the game
 * - a celebratory buzzer noise is played
 */
void trigger_player_win_sequence();
