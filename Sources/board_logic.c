/*
 * Authors: Jermaine Andrade and Jeremy Legere
 *
 * Description: This file contains the implementation for
 * for the board logic component of the system. The functions
 * are based on the modules found in the FRDMK64 board. The following
 * modules are used:
 * - UART
 * - ADC
 * - DAC
 *
 * See board_logic.h for the descriptions.
 */

#include "board_logic.h"
#include <string.h>
#include "MK64F12.h"

uint32_t SINE_WAVE_DATA[] = { 0xFFF, 0xAAA, 0x555, 0x0, 0x555, 0xAAA, 0xFFF };

void put_char(uint8_t char_to_send) {

	// Poll the transmit data register empty flag (TDRE) in the status register (S1)
	// TDRE will be 0 until data is ready
	while (~UART0_S1 & UART_S1_TDRE_MASK) {

	}

	// Set the value to be outputted
	UART0_D = char_to_send;
}

void put_string(char* string_to_send) {
	int i;
	for (i = 0; i < strlen(string_to_send); i++) {
		char cur_char = string_to_send[i];
		put_char(cur_char);
	}
}

void put_stringln(char* string_to_send) {
	int i;
	for (i = 0; i < strlen(string_to_send); i++) {
		char cur_char = string_to_send[i];
		put_char(cur_char);
	}

	put_char(CARRIAGE_RETURN);
	put_char(NEWLINE);
}

uint8_t do_adc_conversion() {
	// Select the channel
	// ADCH bits default to 11111
	ADC1_SC1A &= 0b10010;

	// Poll the COCO flag (bit 7) in ADC1_SC1A
	while (1) {
		if (ADC1_SC1A & ADC_SC1_COCO_MASK) {
			break;
		}
		// Wait for COCO bit to be set
	}
	// Data is ready to be read

	//Read data from ADC1_RA
	uint8_t converted_data = ADC1_RA;
	return converted_data;
}

void init_uart() {
	// Enable the clock for the UART
	SIM_SCGC4 |= SIM_SCGC4_UART0_MASK;

	// Enable the clock for PORTB
	SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;

	// Set PTB16 to UART0_RX
	PORTB_PCR16 |= PORT_PCR_MUX(3);

	// Set PTB17 to UART0_TX
	PORTB_PCR17 |= PORT_PCR_MUX(3);

	// Configure the UART0 for 8-bits
	// Clear bit 4 of UART0 Control Register 1
	UART0_C1 &= 0xE << 4;

	// Disable the transmitter and receiver
	UART0_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);

	// Set the baud rate
	UART0_BDH = 0;
	UART0_BDL |= 0x88;

	// Re-enable the transmitter and receiver
	UART0_C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
}

void init_adc() {
	// Enable the clock for ADC1
	SIM_SCGC3 |= SIM_SCGC3_ADC1_MASK;

	// Enable clock for Port E
	SIM_SCGC5 |= SIM_SCGC5_PORTE_MASK;

	// Do not need to use for MUX for
	// PTE17 as the default config is for ADC1
}

void init_ptc16_input() {

	// Enable clock for PORTC
	SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;

	// Set PTC16 to ALT1 (GPIO)
	PORTC_PCR16 |= PORT_PCR_MUX(1);
	//uint32_t testing = PORTA_PCR0;

	// Set data direction of PTC16 to be input
	GPIOC_PDDR &= 0b0 << 16;

	// Enable pulldown resistor on PTC16
	// Default when enabled is pulldown (0)
	PORTC_PCR16 |= PORT_PCR_PE_MASK;
}

uint8_t read_ptc16() {
	// Read the GPIOA input
	uint32_t ptc16_input = GPIOC_PDIR;

	// Mask all values with 0 except for Pin 0
	ptc16_input = ptc16_input & 0x10000;
	ptc16_input = ptc16_input >> 16;

	return ptc16_input;
}

void init_dac0() {

	// Enable the clock for DAC0
	SIM_SCGC2 |= SIM_SCGC2_DAC0_MASK;

	// Enable DAC0
	DAC0_C0 |= DAC_C0_DACEN_MASK;

	// select the reference voltage
	DAC0_C0 |= (0x01 << 6);

	// Leaving DACBFEN as 0 means that the first
	// word of the buffer is read
}


void do_dac_conversion(int output_count) {
	// Enable DAC0
	DAC0_C0 |= DAC_C0_DACEN_MASK;

	int i;
	for (i = 0; i < output_count; i++) {
		int j;
		for (j = 0; j < TOTAL_SINE_DATA_POINTS; j++) {
			DAC0_DAT0L = SINE_WAVE_DATA[j] & 0xFF;
			DAC0_DAT0H = (SINE_WAVE_DATA[j] & 0xF00) >> 8;

			delay();
		}
	}

	// Disable DAC0
	DAC0_C0 &= ~DAC_C0_DACEN_MASK;
}

void delay() {
	int i;
	for (i = 0; i < DELAY_COUNT; i++) {
		// DO NOTHING
	}
}



