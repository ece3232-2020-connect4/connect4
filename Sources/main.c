/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Authors: Jermaine Andrade and Jeremy Legere
 *
 * Description: This file contains the implementation for
 * for the game logic component of the system. It follows the rules of the
 * traditional Connect 4 game.
 *
 * See game_logic.h for the descriptions.
 */

#include "MK64F12.h"
#include "board_logic.h"
#include "game_logic.h"
#include <string.h>
#include <stdio.h>

/* Game state information */
int game_board[BOARD_HEIGHT][BOARD_WIDTH] = { 0 };
int total_num_turns = 1;
int current_player = 1;
int is_game_finished = 0;

void init_game() {
	init_adc();
	init_uart();
	init_ptc16_input();
	init_dac0();

	display_game_banner();
}

void display_game_banner() {
	put_stringln("===== Connect 4 =====");
}

void display_game_board() {
	int row;
	for (row = 0; row < BOARD_HEIGHT; row++) {
		int col;
		for (col = 0; col < BOARD_WIDTH; col++) {
			char output[5];
			sprintf(output, "%d ", game_board[row][col]);
			put_string(output);
		}
		put_stringln("");
	}
	put_stringln("");
}

void display_game_state() {
	put_stringln("");

	char player_turn_msg[20];
	sprintf(player_turn_msg, "Player %d's Turn", current_player);
	put_stringln(player_turn_msg);

	char total_turn_msg[20];
	sprintf(total_turn_msg, "Turn #%d", total_num_turns);
	put_stringln(total_turn_msg);

	display_game_board();
}

void display_win_message() {
	char win_msg[50];
	sprintf(win_msg, "Player %d has won the game!", current_player);
	put_stringln(win_msg);
}

int determine_selected_column(unsigned char voltage) {
	int column = -1;

	if (voltage >= 0 && voltage <=36 ) {
		column = 0;
	}
	else if (voltage > 36 && voltage <= 72) {
		column = 1;
	}
	else if (voltage > 72 && voltage <= 108) {
		column = 2;
	}
	else if (voltage > 108 && voltage <= 144) {
		column = 3;
	}
	else if (voltage > 144 && voltage <= 180) {
		column = 4;
	}
	else if (voltage > 180 && voltage <= 216) {
		column = 5;
	}
	else if (voltage > 216 && voltage <= 255) {
		column = 6;
	}

	return column;
}

int check_column_select_pressed() {
	uint8_t port_value = read_ptc16();
	if (port_value == 0x01) {
		return 1;
	}
	else {
		return 0;
	}
}

position_t determine_chosen_move(int column) {
	int row;
	int target_row = -1;
	for (row = 0; row < BOARD_HEIGHT; row++) {
		int position_available = game_board[row][column] == 0;

		if (position_available) {
			target_row = row;
		}
	}

	position_t move = { target_row, column };
	return move;
}

int check_valid_move(position_t position) {
	if (position.row < 0 || position.row > BOARD_HEIGHT) {
		return 0;
	}

	if (position.column < 0 || position.column > BOARD_WIDTH) {
		return 0;
	}

	return 1;
}

int get_column_choice() {
	int cur_column = -1;
	int prev_column = -1;
	int select_button_pressed = 0;

	do {
		select_button_pressed = check_column_select_pressed();
		uint8_t data = do_adc_conversion();
		cur_column = determine_selected_column(data);

		if (cur_column != prev_column) {
			char current_selection_msg[16];
			sprintf(current_selection_msg, "Column choice: %d", cur_column);
			put_stringln(current_selection_msg);
		}
		prev_column = cur_column;
	}
	while (!select_button_pressed);

	return cur_column;
}

position_t take_player_turn() {
	int valid_move_picked = 0;
	position_t position;


	while (!valid_move_picked) {
		int column = get_column_choice();
		position = determine_chosen_move(column);
		valid_move_picked = check_valid_move(position);

		if (valid_move_picked) {
			char message[100];
			sprintf(message, "Valid move: [row = %d, col = %d]", position.row, position.column);
			put_stringln(message);

			total_num_turns++;
		}
		else {
			char message[100];
			sprintf(message, "Invalid move: [row = %d, col = %d]", position.row, position.column);
			put_stringln(message);
		}
	}

	return position;
}

void update_game_board(position_t move) {
	game_board[move.row][move.column] = current_player;
}


int check_win_conditions() {

	// horizontalCheck
	for (int j = 0; j<BOARD_HEIGHT-3 ; j++ ){
		for (int i = 0; i<BOARD_WIDTH; i++){
			if (game_board[i][j] == current_player && game_board[i][j+1] == current_player && game_board[i][j+2] == current_player && game_board[i][j+3] == current_player){
				return 1;
			}
		}
	}
	// verticalCheck
	for (int i = 0; i<BOARD_WIDTH-3 ; i++ ){
		for (int j = 0; j<BOARD_HEIGHT; j++){
			if (game_board[i][j] == current_player && game_board[i+1][j] == current_player && game_board[i+2][j] == current_player && game_board[i+3][j] == current_player){
				return 1;
			}
		}
	}
	// ascendingDiagonalCheck
	for (int i=3; i<BOARD_WIDTH; i++){
		for (int j=0; j<BOARD_HEIGHT-3; j++){
			if (game_board[i][j] == current_player && game_board[i-1][j+1] == current_player && game_board[i-2][j+2] == current_player && game_board[i-3][j+3] == current_player)
				return 1;
		}
	}
	// descendingDiagonalCheck
	for (int i=3; i<BOARD_WIDTH; i++){
		for (int j=3; j<BOARD_HEIGHT; j++){
			if (game_board[i][j] == current_player && game_board[i-1][j-1] == current_player && game_board[i-2][j-2] == current_player && game_board[i-3][j-3] == current_player)
				return 1;
		}
	}
	return 0;
}

void play_buzzer() {
	int threeSeconds = 2500;
	do_dac_conversion(threeSeconds);
}

void wait_game_reset() {
	put_stringln("Press the column select button to reset the game.");
	while(!check_column_select_pressed()) { }
}


void reset_game_state() {

	// Reset the game board
	for (int i = 0; i < BOARD_WIDTH; i++){
		for (int j = 0; j < BOARD_HEIGHT; j++){
			game_board[i][j] = 0;
		}
	}

	total_num_turns = 1;
	current_player = 1;
	is_game_finished = 0;

	put_stringln("Game has been reset. Starting new game.");
	put_stringln("--------------------------------------");
}

void trigger_player_win_sequence() {
	display_game_state();
	display_win_message();
	play_buzzer();
}

int main(void) {

	init_game();

	while(1) {
		while (!is_game_finished) {

			display_game_state();
			position_t player_move = take_player_turn();
			update_game_board(player_move);
			int has_a_player_won = check_win_conditions();

			if (has_a_player_won) {
				trigger_player_win_sequence();
				is_game_finished = 1;
				break;
			}

			current_player = current_player == 1 ? 2 : 1;
		}

		wait_game_reset();
		reset_game_state();
	}

    return 0;
}
